/*
 * Copyright 2019 Sebastian Silva
 * Based on example 2018 Daniel Anderson
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

const sdl2link = require('sdl2-link');
const SDL = sdl2link()
    .withFastcall(require('fastcall'))
    .withTTF()
    .load();

require('./engine.js')

rectFill = rectStroke = sprite = () => 0;
clamp = require('lodash/clamp')
range = require('lodash/range')
flatten = require('lodash/flatten')
random = require('lodash/random')
clamp = require('lodash/clamp')

require('./game/code.js')


let gWindowPtr;
let gRendererPtr;
var keys = {};
var state = undefined;
var time = (new Date()).getTime();
var elapsed = 0;

function setup() {
    // Initialize SDL video.
    SDL.SDL_Init(SDL.SDL_INIT_VIDEO);
    SDL.TTF_Init();

    // Allocate buffers for the window and renderer references. In C terms, these buffers are analogous to a SDL_Window**
    // and SDL_Renderer**.
    const windowPtrPtr = SDL.ref.alloc('void*');
    const rendererPtrPtr = SDL.ref.alloc('void*');

    // Create an SDL window.
    SDL.SDL_CreateWindowAndRenderer(
        128,
        128,
        SDL.SDL_WindowFlags.SDL_WINDOW_OPENGL + SDL.SDL_WindowFlags.SDL_WINDOW_RESIZABLE,
        windowPtrPtr,
        rendererPtrPtr);

    // Dereference the window and renderer buffers so they are usable by SDL functions. In C terms, these buffers are now
    // analogous to SDL_Window* and SDL_Renderer*.
    gWindowPtr = windowPtrPtr.deref();
    gRendererPtr = rendererPtrPtr.deref();

    // Init the game engine
    initEngine(SDL, gWindowPtr, gRendererPtr);
}

function shutdown() {
    // Clean up renderer and window.
    SDL.SDL_DestroyRenderer(gRendererPtr);
    SDL.SDL_DestroyWindow(gWindowPtr);

    // Clean up SDL.
    SDL.SDL_Quit();
}


function loop() {
    //
    // Stop the loop when a quit event (window closed) is received.
    if(SDL.SDL_QuitRequested()) {
        shutdown();
        return;
    }

    // Schedule the next frame @ 60 fps.
    setTimeout(loop, 1000 / 60);
    time = (new Date()).getTime();

    if (state === undefined) {
        state = initialState;
    }

    const event = new SDL.SDL_Event();

    while (SDL.SDL_PollEvent(event.ref())) {
        if (event.type === SDL.SDL_EventType.SDL_KEYDOWN) {
            scancode = event.key.keysym.scancode
            keys[scancode] = 1
        }
        else if (event.type === SDL.SDL_EventType.SDL_KEYUP) {
            scancode = event.key.keysym.scancode
            keys[scancode] = 0
        }
        else if (event.type === SDL.SDL_EventType.SDL_WINDOWEVENT) {
            if (event.window.event==SDL.SDL_WindowEventID.SDL_WINDOWEVENT_RESIZED) {
                var w = event.window.d1, 
                    h = event.window.d2;
                SDL.SDL_RenderSetScale(gRendererPtr, w / 128, h / 128)
            }
        }
    }
    let input = { a: keys[SDL.SDL_Scancode.SDL_SCANCODE_A],
                  b: keys[SDL.SDL_Scancode.SDL_SCANCODE_Z],
                  left: keys[SDL.SDL_Scancode.SDL_SCANCODE_LEFT],
                  right: keys[SDL.SDL_Scancode.SDL_SCANCODE_RIGHT],  
                  up: keys[SDL.SDL_Scancode.SDL_SCANCODE_UP],
                  down: keys[SDL.SDL_Scancode.SDL_SCANCODE_DOWN],
                  select: keys[SDL.SDL_Scancode.SDL_SCANCODE_SPACE],
                  enter: keys[SDL.SDL_Scancode.SDL_SCANCODE_ENTER],
    };
    update(state, input, elapsed);
    draw(state);

    // Fill drawing area with blue.
    // SDL.SDL_SetRenderDrawColor(gRendererPtr, 0, 0, 200, 255);
    // SDL.SDL_RenderClear(gRendererPtr);

    // Present the frame on screen.
    SDL.SDL_RenderPresent(gRendererPtr);
    elapsed = Number((new Date()).getTime() - time);
}

setup();
setTimeout(loop, 50);
loop();
