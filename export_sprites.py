from math import floor
import json
from PIL import Image

palette = [
    [246, 214, 189],
    [195, 163, 138],
    [153, 117, 119],
    [129, 98, 113],
    [78, 73, 95],
    [32, 57, 79],
    [15, 42, 63],
    [8, 20, 30]]

width = 128
height = 64

fb = [[None for i in range(0, 128)] for i in range(0, 128)]
with open('game/sprites.json') as json_data:
    d = json.load(json_data)
    for item in range(0, 128):
        x = y = 0
        offset_x = 8 * item % width
        offset_y = 8 * floor(item / 16)
        data = d.get(str(item))
        if data:
            for y in range(0, 8):
                line = data[y]
                for x in range(0, 8):
                    pix = line[x]
                    if pix != ' ':
                        fb[x + offset_x][y + offset_y] = pix


def color(i):
    return tuple(palette[i % len(palette)])


# PIL accesses images in Cartesian co-ordinates, so it is Image[columns, rows]
img = Image.new('RGBA', (width, height), (0,0,0,0))  # create a new image
pixels = img.load()  # create the pixel map

for x in range(img.size[0]):    # for every col:
    for y in range(img.size[1]):    # For every row
        if fb[x][y]:
            pixels[x, y] = color(int(fb[x][y]))  # set the colour accordingly

img.save('sprites.png')
