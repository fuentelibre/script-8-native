## This is a native interpreter for Script-8 games.

1. Install dependencies first (yarn...)

2. Place your script-8 game under `game/` directory

3. Run `node ./script-8-native.js`

### Work in progress

Implemented:
    - line
    - print
    - scaled window
    - input
    - utilities (clamp, random)

Missing:
    - all other shapes
    - sprites
    - sounds
    - map
    - camera
