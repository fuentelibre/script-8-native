// TODO: consolidate - avoid duplication

let triplets = [
  [246, 214, 189],
  [195, 163, 138],
  [153, 117, 119],
  [129, 98, 113],
  [78, 73, 95],
  [32, 57, 79],
  [15, 42, 63],
  [8, 20, 30]
]

// const hexes = [
//   '#f6d6bd',
//   '#c3a38a',
//   '#997577',
//   '#816271',
//   '#4e495f',
//   '#20394f',
//   '#0f2a3f',
//   '#08141e'
// ]

colors = {
  rgb (i) {
    var t = triplets[i % triplets.length]
    return 'rgb(' + t[0] + ',' + t[1] + ',' + t[2] + ')'
  },
  triplet (i) {
    return triplets[i % triplets.length]
  }
}
