from math import floor
import json
from PIL import Image
from colors import palette

width = 128
height = 64


def xy_to_spritenum(x, y):
    dx = floor(x / 8)
    dy = floor(y / 8)
    return 8 * dy + dx


img = Image.open("sprites.png")
pixels = img.load()  # create the pixel map

sprite_obj = {}
for dy in range(8):     # For every row
    for dx in range(16):  # for every col:
        lines = []
        for y in range(8):
            line = ''
            for x in range(8):
                color = pixels[8 * dx + x, 8 * dy + y]
                if color[3] > 0 :
                    line += str(palette.index(list(color[:3])))
                else:
                    line += ' '
            lines.append(line)
        sprite_obj[16 * dy + dx] = lines

with open('result.json', 'w') as fp:
    json.dump(sprite_obj, fp)

