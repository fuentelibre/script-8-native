require('./colors.js')

var SDL, gWindowPtr, gRendererPtr, VT323 = undefined;

function getTextureSize(texturePtr) {
    // taken from https://github.com/dananderson/sdl2-link/blob/master/examples/render-text.js

    const widthPtr = SDL.ref.alloc('int'), heightPtr = SDL.ref.alloc('int');

    SDL.SDL_QueryTexture(texturePtr, null, null, widthPtr, heightPtr);

    return { width: widthPtr.deref() , height: heightPtr.deref() };
}

initEngine = (sdl, win, renderer) => {
    SDL = sdl
    gWindowPtr = win
    gRendererPtr = renderer
    VT323 = SDL.TTF_OpenFont(SDL.toCString("VT323-Regular.ttf"), 9);
}

print = (x, y, letters) => {
    // Render a string to a texture.
    const messageSurfacePtr = SDL.TTF_RenderText_Solid(VT323, SDL.toCString(letters), 0xFFFFFFFF);
    gMessageTexturePtr = SDL.SDL_CreateTextureFromSurface(gRendererPtr, messageSurfacePtr);
    
    const { width, height } = getTextureSize(gMessageTexturePtr);
    const destRect = new SDL.SDL_Rect({x: x, y: y-3, w: width, h: height });
    SDL.SDL_RenderCopy(gRendererPtr, gMessageTexturePtr, null, destRect.ref());
}

clear = (color) => {
    if (color===undefined) {
        color = 7
    }
    rgb = colors.triplet(color)
    SDL.SDL_SetRenderDrawColor(gRendererPtr, rgb[0], rgb[1], rgb[2], 255);
    SDL.SDL_RenderClear(gRendererPtr);
}

line = (x1, y1, x2, y2, color) => {
    rgb = colors.triplet(color)
    SDL.SDL_SetRenderDrawColor(gRendererPtr, rgb[0], rgb[1], rgb[2], 255);
    SDL.SDL_RenderDrawLine(gRendererPtr, x1, y1, x2, y2)
}
